---
author: 'Team 8'

course: 7COM1079

institution: University of Hertfordshire

module: 7COM1079

subtitle: '7COM1079 -- Team Research and Development Project'
term: 'fall-19'

group members: 'Aadarsa Varna Rajaram', 'Anagha Unnikrishnan', 'Aswathy Lalan', 'Jisna Varghese', 'Mithul Joseph Mathew'

---
